﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    public float speed = 15.0f;

    [SerializeField]
    private bool isRotating, followPlayer, gamerunning;

    [SerializeField]
    private float randomvalue;

    [SerializeField]
    private GameObject player;

    public GameObject _GM;

    private void OnEnable()
    {
        GameManager.GameIsOver += EndGame;
        GameManager.GameStart += GameStart;
        randomvalue = Random.Range(0.1f, 1f);
    }

    void Start()
    {
        gamerunning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gamerunning == true)
        {

            if (followPlayer == false)
            {
                transform.position += transform.TransformDirection(new Vector3(speed * Time.deltaTime, 0, 0));
            }
            else if (followPlayer == true)
            {
                float step = speed * Time.deltaTime;
               // Vector3 LookPos = new Vector3(player.transform.position.x,player.transform.position.y,0);
                //transform.LookAt(LookPos);
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, step);
            }

            RaycastHit2D wallhit = Physics2D.Raycast(transform.position, transform.right, .5f);
            RaycastHit2D PlayerHit = Physics2D.Raycast(transform.position, transform.right, 4f);
            Debug.DrawRay(transform.position, transform.right * .5f, Color.red);
            Debug.DrawRay(transform.position, transform.right * 2, Color.green);
            if (wallhit.collider != null)
            {
                if (wallhit.collider.transform.name.Contains("House") || wallhit.collider.transform.name.Contains("Map Bound") && isRotating == false)
                {

                    Debug.Log("Vuelta");
                    isRotating = true;
                    if (randomvalue < 0.5f)
                    {
                        transform.eulerAngles = transform.eulerAngles + new Vector3(0, 0, 90);

                    }
                    else
                    {
                        transform.eulerAngles = transform.eulerAngles + new Vector3(0, 0, -90);
                    }
                    StartCoroutine(booleanRotation());
                }
            }

            if (PlayerHit.collider != null)
            {
                if (PlayerHit.collider.tag == "Player" && followPlayer == false)
                {

                    FollowPlayer(PlayerHit);

                }
            }
            else
            {
                StartCoroutine(returnToPtratol());
            }

            if (player != null)
            {
                float dis = Vector2.Distance(transform.position, player.transform.position);
                if (dis < .5f && _GM.GetComponent<GameManager>().sugarRush == true)
                {
                    Death();
                }
                else if (dis < .5f && _GM.GetComponent<GameManager>().sugarRush == false)

                {
                    _GM.GetComponent<GameManager>().GameOver();
                }
            }

        }
    }

    IEnumerator returnToPtratol()
    {
        yield return new WaitForSeconds(5);
        followPlayer = false;

    }

    IEnumerator booleanRotation()
    {
        isRotating = false;
        yield return new WaitForSeconds(.5f);
        randomvalue = Random.Range(0.1f, 1f);

    }

    private void FollowPlayer(RaycastHit2D PlayerHit)
    {
        if (_GM.GetComponent<GameManager>().sugarRush == false)
        {
            followPlayer = true;
            player = PlayerHit.transform.gameObject;
            Debug.Log(PlayerHit.collider.transform.name);
        }
        else if (_GM.GetComponent<GameManager>().sugarRush == true)
        {
            transform.eulerAngles = transform.eulerAngles + new Vector3(0, 0, 180);
        }
    }


    private void Death()
    {
        gameObject.SetActive(false);
    }

    public void GameStart()
    {
        gamerunning = true;
    }

    public void EndGame()
    {
        gamerunning = false;
    }
}
