﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    bool _sugarRushActivated;
    float _attackCooldown = 0f;
    float _startAttackCooldown = 0.5f;

    [SerializeField] Transform _attackPos;
    public LayerMask enemyLayer;
    public float attackRange;


    // Start is called before the first frame update
    void Start()
    {
        _attackPos = GetComponentInChildren<PlayerAttack>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(_attackCooldown <= 0 && Input.GetButtonDown("Interact"))
        {
            _attackCooldown = _startAttackCooldown;
            Collider2D[] enemiesHit = Physics2D.OverlapCircleAll(_attackPos.position, attackRange, enemyLayer);
            for (int i = 0; i < enemiesHit.Length; i++)
            {
                enemiesHit[i].gameObject.SetActive(false);
            }
        }
        else
        {
            _attackCooldown -= Time.deltaTime;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_attackPos.position, attackRange);
    }
}
