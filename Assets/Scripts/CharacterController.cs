﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    Rigidbody2D _body;

    float _horizontal;
    float _vertical;
    Vector2 _velocity;
    float _moveLimiter = 0.7f;
    [SerializeField] float _runSpeed = 0f;
    float _buffedSpeed;
    float _normalSpeed;

    float _eatCooldown = 0f;
    float _startEatCooldown = 2f;

    bool _isBuffed;

    Animator _animatorController;

    // Start is called before the first frame update
    void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        _animatorController = GetComponent<Animator>();
        _normalSpeed = _runSpeed;
        _buffedSpeed = _normalSpeed * 2;
    }

    // Update is called once per frame
    void Update()
    {
        _horizontal = Input.GetAxisRaw("Horizontal");
        _vertical = Input.GetAxisRaw("Vertical");

        if(_eatCooldown <= 0 && Input.GetButtonDown("EatCandy"))
        {
            _eatCooldown = _startEatCooldown;
            GameManager.Instance.EatCandy(_startEatCooldown);
        }
        else
        {
            _eatCooldown -= Time.deltaTime;
        }

        if (_isBuffed)
        {
            _runSpeed = _buffedSpeed;
        }
        else
        {
            _runSpeed = _normalSpeed;
        }
    }

    void FixedUpdate()
    {
        if (_horizontal != 0 && _vertical != 0)
        {
            _horizontal *= _moveLimiter;
            _vertical *= _moveLimiter;
        }

        _velocity = new Vector2(_horizontal * _runSpeed, _vertical * _runSpeed);
        _body.MovePosition(_body.position + _velocity * Time.fixedDeltaTime);

        if (_vertical == -1)
        {
            _animatorController.SetBool("DownWalk", true);
            _animatorController.SetBool("RightWalk", false);
            _animatorController.SetBool("UpWalk", false);
            _animatorController.SetBool("LeftWalk", false);
        }
        else if (_horizontal == 1)
        {
            _animatorController.SetBool("RightWalk", true);
            _animatorController.SetBool("DownWalk", false);
            _animatorController.SetBool("UpWalk", false);
            _animatorController.SetBool("LeftWalk", false);
        }
        else if (_horizontal == -1)
        {
            _animatorController.SetBool("LeftWalk", true);
            _animatorController.SetBool("DownWalk", false);
            _animatorController.SetBool("RightWalk", false);
            _animatorController.SetBool("UpWalk", false);
        }
        else if (_vertical == 1)
        {
            _animatorController.SetBool("UpWalk", true);
            _animatorController.SetBool("DownWalk", false);
            _animatorController.SetBool("RightWalk", false);
            _animatorController.SetBool("LeftWalk", false);
        }
        else if (_vertical == 0 && _horizontal == 0)
        {
            _animatorController.SetBool("DownWalk", false);
            _animatorController.SetBool("RightWalk", false);
            _animatorController.SetBool("UpWalk", false);
            _animatorController.SetBool("LeftWalk", false);
        }
    }

    void PlayerBuff()
    {
        _isBuffed = !_isBuffed;
    }

    void OnEnable()
    {
        GameManager.DebuffPlayer += PlayerBuff;
    }

    void OnDisable()
    {
        GameManager.DebuffPlayer -= PlayerBuff;
    }
}
