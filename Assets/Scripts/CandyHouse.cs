﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyHouse : MonoBehaviour
{
    GameObject _canvas;
    int _maxCandy = 5;
    [SerializeField] bool _isOpen = true;
    bool _isButtonPressed = false;
    public Sprite unlitHouse;

    // Start is called before the first frame update
    void Start()
    {
        _canvas = GetComponentInChildren<Canvas>(true).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        _isButtonPressed = Input.GetButtonDown("Interact");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && _isOpen)
        {
            _canvas.SetActive(true);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(_isOpen && _isButtonPressed && !GameManager.Instance.sugarRush)
            {
                GameManager.Instance.AddCandy(Random.Range(1, _maxCandy + 1));
                _isOpen = false;
                gameObject.GetComponent<SpriteRenderer>().sprite = unlitHouse;
                _canvas.SetActive(false);
            }
                
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _canvas.SetActive(false);
        }
    }
}
