﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHouse : MonoBehaviour
{
    GameObject _canvas;
    bool _isButtonPressed = false;

    // Start is called before the first frame update
    void Start()
    {
        _canvas = GetComponentInChildren<Canvas>(true).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        _isButtonPressed = Input.GetButtonDown("Interact");
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (_isButtonPressed && !GameManager.Instance.sugarRush)
            {

                _canvas.SetActive(false);
            }

        }
    }
}
