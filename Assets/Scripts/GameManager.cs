﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public static Action GameIsOver;
    public static Action GameStart;
    public static Action SugarRush;
    public static Action DebuffPlayer;

    [SerializeField]
    private float totalTime = 0f, candyCount = 0f, initialTime = 0f, candyThreshold = 0f;
    private float tempTime, consumedCandies;

    static int countdownTime;

    private bool gameStart;

    public bool sugarRush;

    [SerializeField]
    private Slider powerUPFill;
    [SerializeField]
    private TextMeshProUGUI timerText, countStartText, candyCounterText;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameStart = false;
        sugarRush = false;
        countdownTime = 3;
        tempTime = 0;
        StartCoroutine(TextStart());
        candyCount = 0;

        float offset = -10;
        for (int i = 0; i < 3; i++)
        {
            GameObject enemy = ObjectPooler.Instance.GetObjInPool("Enemy");
            enemy.transform.position = new Vector3(UnityEngine.Random.Range(-12.63f, 12.38f), UnityEngine.Random.Range(-7.49f, 8f), 0);
            enemy.GetComponent<EnemyController>().speed = 15;
            offset += 1.5f;
            enemy.GetComponent<EnemyController>()._GM = this.gameObject;
            enemy.SetActive(true);
        }

        for (int i = 0; i < 10; i++)
        {
            GameObject enemy = ObjectPooler.Instance.GetObjInPool("Candy");
            enemy.transform.position = new Vector3(UnityEngine.Random.Range(-50f, 50f), UnityEngine.Random.Range(-28, 28f), 0);
            enemy.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStart == true)
        {
            if (tempTime < totalTime)
            {
                tempTime += Time.deltaTime;
                timerText.SetText(PrintTime(tempTime));
            }
            else if (tempTime >= totalTime)
            {
                // Stop game
                GameOver();
            }

            if(sugarRush == true)
            {
                SugarRush?.Invoke();
            }

        }

        if(Input.GetKeyDown("q"))
        {
            AddCandy(10);
        }
    }

    public void EatCandy(float duration)
    {
        if (candyCount > 0 && !sugarRush)
        {
            AddCandy(-1);
            consumedCandies++;
            DebuffPlayer?.Invoke();
            CheckThreshold();
            powerUPFill.gameObject.SetActive(true);
            powerUPFill.normalizedValue = 1f;
            powerUPFill.DOValue(0f, duration).OnComplete(FinishBuff);
        }       
    }

    private string PrintTime(float time)
    {
        int minutes = (int) initialTime + (int)time / 60;
        int seconds = (int)time % 60;
        return $"{minutes}:{seconds:00} PM";
    }

    public void AddCandy(int value)
    {
        if (!sugarRush)
        {
            candyCount += value;
            candyCounterText.SetText("x {0}", candyCount);
        }
    }

    public void GameOver()
    {
        gameStart = false;
        countStartText.gameObject.SetActive(true);
        countStartText.SetText("Game Over!");
        GameIsOver?.Invoke();
    }

    public void GameWin()
    {
        gameStart = false;
        countStartText.gameObject.SetActive(true);
        countStartText.SetText("You Win!!!");
        GameIsOver?.Invoke();
    }


    void GameStarts()
    {
        GameStart?.Invoke();
        gameStart = true;
    }

    public void SetSugarRush(bool value)
    {
        sugarRush = value;
    }

    public void CheckThreshold()
    {
        if(consumedCandies >= candyThreshold)
        {
            SetSugarRush(true);
        }
    }

    public void FinishBuff()
    {
        powerUPFill.gameObject.SetActive(false);
        DebuffPlayer?.Invoke();
    }

    IEnumerator TextStart()
    {
        while (countdownTime > 0)
        {
            countStartText.SetText("{0}", countdownTime);
            yield return new WaitForSeconds(1);
            countdownTime--;
        }

        countStartText.SetText("Go!");
        GameStarts();
        yield return new WaitForSeconds(1);
        countStartText.gameObject.SetActive(false);
    }
}
