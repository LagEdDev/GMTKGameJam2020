﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    public int pooledAmount;
    public GameObject pooledObject;
    public string searchTag;
}

public class ObjectPooler : MonoBehaviour
{
    private static ObjectPooler _instance;
    public static ObjectPooler Instance { get { return _instance; } }

    public List<ObjectPoolItem> itemsToPool;
    private List<GameObject> _objectPool = new List<GameObject>();

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _objectPool = new List<GameObject>();

        foreach (ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.pooledAmount; i++)
            {
                GameObject obj = Instantiate(item.pooledObject);
                obj.name = item.searchTag;
                obj.SetActive(false);
                _objectPool.Add(obj);
            }
        }
    }

    public GameObject GetObjInPool(string sTag)
    {
        for (int i = 0; i < _objectPool.Count; i++)
        {
            if (!_objectPool[i].activeInHierarchy && _objectPool[i].name == sTag)
            {
                return _objectPool[i];
            }
        }

        return null;
    }
}
